window.onload = init;

var btns;
var secciones;

function init()
{
   initialize_variables();
   initialize_events();
}

function initialize_variables()
{
   btns = [];
   btns[0] = document.getElementById("section_reloj");
   btns[1] = document.getElementById("section_contador");
   btns[2] = document.getElementById("section_timer");
   btns[3] = document.getElementById("section_regresion");

   secciones = [];
   secciones[0] = document.getElementById("reloj");
   secciones[1] = document.getElementById("contador");
   secciones[2] = document.getElementById("timer");
   secciones[3] = document.getElementById("regresion");

}

function initialize_events()
{
   for(var i in btns)
   {
      btns[i].addEventListener("click",processClick);
   }
}

function processClick(evt)
{
   var str = evt.target.id;
   var section = str.split("_")[1];
   var ref = document.getElementById(section);
   hide();
   ref.className = "animated slideInDown";
}

function hide()
{
   for(var i in secciones)
   {
      secciones[i].className = "hidden";
   }
}

